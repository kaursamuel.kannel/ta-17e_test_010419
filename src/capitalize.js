module.exports=function capitalize(name){
	if(name!==typeof(String)){
		throw new Error("bad input");
	}
	return name.charAt(0).toUpperCase() +name.slice(1);
};